#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

double dtime() {
    double tseconds = 0.0;
    struct timeval mytime;
    gettimeofday(&mytime,(struct timezone*)0);
    tseconds = (double)(mytime.tv_sec + mytime.tv_usec*1.0e-6);
    return ( tseconds );
}

int main(int argc, char* argv[]){
	int i, j, k, n=0, pomocnicza=0;
	double t[3];
	srand(time(NULL));

	if(argc >1){
		n = atoi(argv[1]);
		omp_set_num_threads(atoi(argv[2]));
	}
	else{
		n = 1000;
		omp_set_num_threads(2);
	}

	printf("N wynosi: %i\n", n);

	double **A;
	double **B;
	double **C;

	A=(double **)malloc(sizeof(double *)*n);
	for(i=0;i<n;i++){
    		A[i]=(double *)malloc(sizeof(double)*n);
	}

	B=(double **)malloc(sizeof(double *)*n);
        for(i=0;i<n;i++){
                B[i]=(double *)malloc(sizeof(double)*n);
        }

	C=(double **)malloc(sizeof(double *)*n);
        for(i=0;i<n;i++){
                C[i]=(double *)malloc(sizeof(double)*n);
        }

	t[0] = dtime();

	//WYPELNIANIE MACIERZY A
	for(i=0; i<n; i++){
		for(j=0; j<n; j++){
			A[i][j] = rand()%10;
		}
	}
	//WYPELNIANIE MACIERZY B
	for(i=0; i<n; i++){
		for(j=0; j<n; j++){
			B[i][j] = rand()%10;
		}
	}

	//ZEROWANIE MACIERZY C
	for(i=i=0; i<n; i++){
		for(j=0; j<n; j++){
			C[i][j] = 0;
		}
	}

	/*
	printf("Macierz A\n");
	for(i = 0; i < n; i++){
		for(j = 0; j < n; j++){
			printf("%g ", A[i][j] );
		}
		puts("");
	}
  	puts("");

	printf("Macierz B\n");
	for(i = 0; i < n; i++){
		for(j = 0; j < n; j++){
			printf("%g ", B[i][j] );
		}
		puts("");
	}
	puts("");
	*/

	//MNOZENIE MACIERZY A i B
	t[1] = dtime();

	#pragma omp parallel for shared(C, A, B) private(i,j,k)
	for(i = 0; i < n; i++){
		for(j = 0; j < n; j++){
			for(k = 0; k < n; k++){
				C[i][j] = C[i][j] + A[i][k] * B[k][j];
			}
		}
	}


	t[2] = dtime();

	/*
	//MACIERZ C Z WYNIKIEM MNOZENIA A i B
	printf("Macierz wynikowa C po przemnozeniu A i B\n");
        for(i = 0; i < n; i++) {
                for(j = 0; j < n; j++){
                	printf("%g ", C[i][j] );
		}
                puts("");
        }
        puts("");
	*/

	puts(" ");
	printf("Czas inicjalizacji wynosi: %f \n", t[1] - t[0] );
	printf("Czas mnozenia wynosi: %f \n", t[2] - t[1]);

	return 0;
}
