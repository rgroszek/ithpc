#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <sys/time.h>


double dtime() {
	double tseconds = 0.0;
	struct timeval mytime;
	gettimeofday(&mytime,(struct timezone*)0);
	tseconds = (double)(mytime.tv_sec + mytime.tv_usec*1.0e-6);
	return ( tseconds );
}

void main(int argc, char* argv[]){

	int i, n, a;
	double t[3];
	double *X;
	double *Y;
	srand(time(NULL));

	if(argc > 1){
		n = atoi(argv[1]);
		a = atoi(argv[2]);
		omp_set_num_threads(atoi(argv[3]));
	}
	else{
		n = 1000;
		a = 200;
		omp_set_num_threads(2);
	}
	X = (double *)malloc(sizeof(double)*n);
	Y = (double *)malloc(sizeof(double)*n);

	t[0] = dtime();

	#pragma omp parallel for shared(X, Y) private(i) 
	for(i = 0; i < n; i++){
		X[i] = rand()%10;
		Y[i] = rand()%10;
	}

	t[1] = dtime();

	#pragma omp parallel for shared(a, X, Y) private(i)
	for(i=0; i<n; ++i){
		Y[i] = a * X[i] + Y[i];
	}

	t[2] = dtime();

	puts("");
	printf("Czas inicjalizacji wynosi: %f \n", t[1]-t[0]);
	printf("Czas liczenia wynosi: %f \n", t[2]-t[1]);
}
